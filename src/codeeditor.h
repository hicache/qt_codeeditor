#pragma once

#include <QtWidgets/QPlainTextEdit>

class CodeLineNumArea;
class CodeEditorPrivate;

class CodeEditor : public QPlainTextEdit {
    Q_OBJECT
    Q_PROPERTY(bool highlightCurrentLine READ isHightlightCurrentLine WRITE setHighlightCurrentLine)
public:
    explicit CodeEditor(QWidget* parent = nullptr);
    ~CodeEditor() noexcept override;

    bool isHightlightCurrentLine() const;
    void setHighlightCurrentLine(bool enable);

    void setSyntax(const QString& syntax_toml);

protected:
    void resizeEvent(QResizeEvent* event) override;
    void wheelEvent(QWheelEvent* event) override;
    void keyPressEvent(QKeyEvent* event) override;

private:
    void paintLineNumArea(QPaintEvent* event);
    int lineNumAreaWidth() const;

private:
    friend class CodeLineNumArea;
    friend class CodeEditorPrivate;
    CodeEditorPrivate* d_ { nullptr };
};