#include "codeeditor.h"
#include <QDebug>
#include <QtCore/QRegularExpression>
#include <QtGui/QPainter>
#include <QtGui/QSyntaxHighlighter>
#include <QtGui/QTextBlock>
#include <toml.hpp>

struct CodeSyntaxItem {
    QString pattern;
    QTextCharFormat format;
};

class CodeSyntaxHighlighter : public QSyntaxHighlighter {

public:
    explicit CodeSyntaxHighlighter(QTextDocument* document)
        : QSyntaxHighlighter(document)
    {
    }

    void setSyntax(const QVector<CodeSyntaxItem>& syntax_items)
    {
        syntax_items_ = syntax_items;
        rehighlight();
    }

protected:
    void highlightBlock(const QString& text) override
    {
        for (const auto& syntax : syntax_items_) {
            QRegularExpression syntax_re(syntax.pattern);
            auto it = syntax_re.globalMatch(text);
            while (it.hasNext()) {
                auto match = it.next();
                setFormat(match.capturedStart(), match.capturedLength(), syntax.format);
            }
        }

        QRegularExpression numlti_comment_begin(R"("""|'''')");
        QRegularExpression numlti_comment_end(R"("""|''')");

        QTextCharFormat comment_format;
        comment_format.setForeground(Qt::darkGreen);

        setCurrentBlockState(0);

        int comment_start_idx = 0;
        QRegularExpressionMatch start_match;
        if (previousBlockState() != 1) {
            comment_start_idx = text.indexOf(numlti_comment_begin, 0, &start_match);
        }

        while (comment_start_idx >= 0) {
            QRegularExpressionMatch match;

            int comment_end_idx
                = text.indexOf(numlti_comment_end, comment_start_idx + start_match.capturedLength(), &match);
            int comment_length;

            if (comment_end_idx == -1) {
                setCurrentBlockState(1);
                comment_length = text.length() - comment_start_idx;
            } else {
                comment_length = comment_end_idx - comment_start_idx + match.capturedLength();
            }

            setFormat(comment_start_idx, comment_length, comment_format);

            comment_start_idx = text.indexOf(numlti_comment_begin, comment_start_idx + comment_length);
        }

        QRegularExpression line_comment_re(R"(^\s*#.*$)");
        QRegularExpressionMatch line_comment_match;
        int line_comment_start_idx = text.indexOf(line_comment_re, 0, &line_comment_match);
        if (line_comment_start_idx != -1) {
            setFormat(line_comment_match.capturedStart(), line_comment_match.capturedLength(), comment_format);
        }
    }

private:
    QVector<CodeSyntaxItem> syntax_items_;
};

class CodeLineNumArea : public QWidget {
public:
    explicit CodeLineNumArea(CodeEditor* parent)
        : QWidget(parent)
        , editor_(parent)
    {
        resize(editor_->lineNumAreaWidth(), 0);
    }

protected:
    void paintEvent(QPaintEvent* event) override
    {
        editor_->paintLineNumArea(event);
    }

private:
    CodeEditor* editor_ { nullptr };
};

class CodeEditorPrivate {
public:
    explicit CodeEditorPrivate(CodeEditor* parent)
        : q_(parent)
        , linenum_area_(new CodeLineNumArea(parent))
        , syntax_hl_(new CodeSyntaxHighlighter(parent->document()))
    {
    }

    void highlightCurrentLine()
    {
        if (hl_curr_line_) {
            QList<QTextEdit::ExtraSelection> extraSelections;
            if (!q_->isReadOnly()) {
                QTextEdit::ExtraSelection selection;
                QColor lineColor = QColor(Qt::yellow).lighter(160);
                selection.format.setBackground(lineColor);
                selection.format.setProperty(QTextFormat::FullWidthSelection, true);
                selection.cursor = q_->textCursor();
                selection.cursor.clearSelection();
                extraSelections.append(selection);
            }
            q_->setExtraSelections(extraSelections);
        }
    }

    void updateLineNumAreaWidth()
    {
        q_->setViewportMargins(q_->lineNumAreaWidth(), 0, 0, 0);
    }

    void updateLineNumArea(const QRect& rect, int dy)
    {
        if (dy != 0) {
            linenum_area_->scroll(0, dy);
        } else {
            linenum_area_->update(0, rect.y(), q_->lineNumAreaWidth(), rect.height());
        }
        if (rect.contains(q_->viewport()->rect())) {
            updateLineNumAreaWidth();
        }
    }

private:
    friend class CodeEditor;
    friend class CodeLineNumArea;
    CodeEditor* q_ { nullptr };
    CodeLineNumArea* linenum_area_ { nullptr };
    CodeSyntaxHighlighter* syntax_hl_ { nullptr };
    bool zoom_enable_ { true };
    bool hl_curr_line_ { true };
};

CodeEditor::CodeEditor(QWidget* parent)
    : QPlainTextEdit(parent)
    , d_(new CodeEditorPrivate(this))
{
    connect(this, &CodeEditor::cursorPositionChanged, [this] { d_->highlightCurrentLine(); });
    connect(this, &CodeEditor::updateRequest, [this](const QRect& rect, int dy) { d_->updateLineNumArea(rect, dy); });
    connect(this, &CodeEditor::blockCountChanged, [this] { d_->updateLineNumAreaWidth(); });
    d_->highlightCurrentLine();
    d_->updateLineNumAreaWidth();
}

CodeEditor::~CodeEditor() noexcept
{
    delete d_;
}

void CodeEditor::resizeEvent(QResizeEvent* event)
{
    QPlainTextEdit::resizeEvent(event);
    d_->linenum_area_->setGeometry(0, 0, lineNumAreaWidth(), event->size().height());
}

void CodeEditor::wheelEvent(QWheelEvent* event)
{
    if (!d_->zoom_enable_) {
        QPlainTextEdit::wheelEvent(event);
    } else {
        if (event->modifiers().testFlag(Qt::KeyboardModifier::ControlModifier)) {
            if (event->angleDelta().y() > 0) {
                zoomIn();
            } else {
                zoomOut();
            }
        } else {
            QPlainTextEdit::wheelEvent(event);
        }
    }
}

void CodeEditor::keyPressEvent(QKeyEvent* event)
{
    // 键入TAB后插入4个空格
    if (event->modifiers().testFlag(Qt::KeyboardModifier::NoModifier) && event->key() == Qt::Key::Key_Tab) {
        insertPlainText("    ");
    } else {
        QPlainTextEdit::keyPressEvent(event);
    }
}

void CodeEditor::paintLineNumArea(QPaintEvent* event)
{
    QPainter painter(d_->linenum_area_);
    painter.fillRect(event->rect(), Qt::lightGray);
    QRect cr = contentsRect().translated(lineNumAreaWidth() - 1, 0);
    painter.setPen(Qt::black);
    painter.drawLine(cr.topLeft(), cr.bottomLeft());

    QTextBlock block = firstVisibleBlock();
    int block_num = block.blockNumber();

    int top = qRound(blockBoundingRect(block).translated(contentOffset()).top());
    int bottom = top + qRound(blockBoundingRect(block).height());
    int width = lineNumAreaWidth() - 5;
    int height = fontMetrics().height();
    while (block.isValid() && top <= event->rect().bottom()) {
        if (block.isVisible() && bottom > event->rect().top()) {
            painter.drawText(0, top, width, height, Qt::AlignRight, QString::number(block_num + 1));
        }
        block = block.next();
        top = bottom;
        bottom = top + qRound(blockBoundingRect(block).height());
        ++block_num;
    }
}

int CodeEditor::lineNumAreaWidth() const
{
    int digits = qMax(1, QString::number(document()->blockCount()).size());
    return 10 + fontMetrics().boundingRect(QLatin1Char('@')).width() * digits;
}

bool CodeEditor::isHightlightCurrentLine() const
{
    return d_->hl_curr_line_;
}

void CodeEditor::setHighlightCurrentLine(bool enable)
{
    d_->hl_curr_line_ = enable;
    d_->highlightCurrentLine();
}

void CodeEditor::setSyntax(const QString& syntax_toml)
{
    QVector<CodeSyntaxItem> syntax_items;
    try {
        const auto data = toml::parse(syntax_toml.toStdString());

        const auto& keyword = toml::find(data, "keyword");
        const auto patterns = toml::find<std::vector<std::string>>(keyword, "pattern");
        const auto type = toml::find<std::string>(keyword, "type");
        const auto color = toml::find<uint32_t>(keyword, "color");

        CodeSyntaxItem syntax_item;

        if (type == "word") {
            QStringList pattern_list;
            std::transform(patterns.begin(), patterns.end(), std::back_inserter(pattern_list),
                [](const std::string& p) { return QString::fromStdString(p); });
            syntax_item.pattern = "\\b" + pattern_list.join("\\b|\\b") + "\\b";
            syntax_item.format.setForeground(QColor::fromRgb(color));
            syntax_item.format.setFontWeight(toml::find_or<bool>(keyword, "bold", false) ? QFont::Bold : QFont::Normal);
            syntax_item.format.setFontItalic(toml::find_or<bool>(keyword, "italic", false));
        }

        syntax_items.append(syntax_item);
        d_->syntax_hl_->setSyntax(syntax_items);
    } catch (...) {
        qDebug() << "Failed to parse " << syntax_toml;
    }
}