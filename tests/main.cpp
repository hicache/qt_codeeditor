#include "codeeditor.h"
#include <QtCore/QDebug>
#include <QtWidgets/QApplication>

int main(int argc, char* argv[])
{
    QApplication app(argc, argv);

    QString syntax_toml_path = CURRENT_ROOT_DIR "/py.toml";

    CodeEditor editor;
    editor.resize(640, 480);
    editor.show();
    editor.setSyntax(syntax_toml_path);

    editor.setPlainText(R"(
#include <iostream>

int main()
{
    std::cout << "Hello world!" << std::endl;
    return 0;
}
    )");

    editor.setPlainText(R"(
class Base:
    """
    Base class
    """
    def __init__(self):
        pass

    def set(self):
        # for loop
        for i in range(6):
            print(i)
    )");

    return app.exec();
}